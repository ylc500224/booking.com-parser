from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import time
import csv
import pandas as pd

def main(output_file_name, url):
    browser = webdriver.Chrome()
    browser.get(url)
    bs = BeautifulSoup(browser.page_source, 'html.parser')
    total_pages = len(bs.findAll('div',{'class':'bui-u-inline'})) # get 頁數
    print('Total pages: ', total_pages)
    
    price, name, points, con1, con2 = [],[],[],[],[]
    for page_i in range(total_pages):
        bs = BeautifulSoup(browser.page_source, 'html.parser')
        # --------------------- start content parser --------------------- #
        for i in bs.findAll('div', {'class':'sr_item_content'}):

            # 價格
            if i.find('div',{'class':'bui-price-display__value'}) != None:
                d = i.find('div',{'class':'bui-price-display__value'}).text[1:-1].split('\xa0')[-1]
                price.append(int((d).replace(',', '')))

                # 名稱
                if i.find('span',{'class':'sr-hotel__name'}) != None:
                    name.append(i.find('span',{'class':'sr-hotel__name'}).text[1:-1])

                # 評分
                if i.find('div',{'class':'bui-review-score__badge'}) != None:
                    points.append(float(i.find('div',{'class':'bui-review-score__badge'}).text[1:-1]))
                else:
                    points.append(None)

                # 過去訂單狀況
                if i.find('div',{'class':'rollover-s2'}) != None:
                    con1.append(i.find('div',{'class':'rollover-s2'}).text[1:-1])
                else:
                    con1.append(None)

                if i.find('div',{'class':'lastbooking'}) != None:
                    con2.append(i.find('div',{'class':'lastbooking'}).text[1:-1])
                else:
                    con2.append(None)
            # else:
             #   print('剛剛售出') # 售出就沒價格資料可爬
        # ---------------------------------------------------------------- #

        # 下一頁
        browser.find_element_by_class_name("bui-pagination__item.bui-pagination__next-arrow").click()
        time.sleep(2)
        print((page_i+1)/total_pages*100, '%',end='\r') # print out 完成 %數

    # 做成 dataframe
    df = pd.DataFrame()
    df['名稱']=name
    df['價格']=price
    df['評分']=points
    df['過去訂房狀況1']=con1
    df['過去訂房狀況2']=con2
    
    if output_file_name != 'NO':
        df.to_csv(output_file_name, index=False)
    
    print('\n房間數: ', len(df), '間')
    print('平均價格: ', round(df['價格'].mean(), 1), '元')
    print('中位數: ', round(df['價格'].median(), 1), '元')
    print('眾數: ', df['價格'].mode()[0], '元')
    
if __name__ == '__main__':

	# 輸出檔名，格式如: '20190706.csv'
	output_file_name = 'NO' # 如果不要產生 csv file 請輸入字串 'NO'
	# 放要爬資料的網址
	url = 'https://www.booking.com/searchresults.zh-tw.html?label=gen173nr-1FCAQoggI49ANIMFgEaOcBiAEBmAEwuAEXyAEM2AEB6AEB-AECiAIBqAIDuAKDz_foBcACAQ&sid=41add819f07e83c0a001bd52c5da4bdb&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.zh-tw.html%3Flabel%3Dgen173nr-1FCAQoggI49ANIMFgEaOcBiAEBmAEwuAEXyAEM2AEB6AEB-AECiAIBqAIDuAKDz_foBcACAQ%3Bsid%3D41add819f07e83c0a001bd52c5da4bdb%3Btmpl%3Dsearchresults%3Bcheckin_month%3D7%3Bcheckin_monthday%3D10%3Bcheckin_year%3D2019%3Bcheckout_month%3D7%3Bcheckout_monthday%3D17%3Bcheckout_year%3D2019%3Bcity%3D-2631690%3Bclass_interval%3D1%3Bdest_id%3D-2631690%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D3%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D2%3Boffset%3D0%3Bpostcard%3D0%3Broom1%3DA%3Broom2%3DA%252CA%3Bsb_price_type%3Dtotal%3Bshw_aparth%3D1%3Bslp_r_match%3D0%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bsrpvid%3D6cd953a33ca80095%3Bss%3D%25E8%258A%25B1%25E8%2593%25AE%25E5%25B8%2582%3Bss_all%3D0%3Bssb%3Dempty%3Bsshis%3D0%3Bssne%3D%25E8%258A%25B1%25E8%2593%25AE%25E5%25B8%2582%3Bssne_untouched%3D%25E8%258A%25B1%25E8%2593%25AE%25E5%25B8%2582%26%3B&ss=%E8%8A%B1%E8%93%AE%E5%B8%82&is_ski_area=0&ssne=%E8%8A%B1%E8%93%AE%E5%B8%82&ssne_untouched=%E8%8A%B1%E8%93%AE%E5%B8%82&city=-2631690&checkin_year=2019&checkin_month=7&checkin_monthday=4&checkout_year=2019&checkout_month=7&checkout_monthday=5&group_adults=2&group_children=0&no_rooms=1&from_sf=1'

    main(output_file_name, url)
    print('finish!')